function matchesperyear(y){
    let newarray = {}
    for(let i=0; i<y.length;i++){
        if (y[i].season in newarray){
            newarray[y[i].season] += 1
        }else{
            newarray[y[i].season] = 1
        }
    }
    return newarray;
}




function matchesownperyear(x){
    let matchesown={}
    for(i=0;i<x.length;i++){
        if (x[i].season in matchesown){
            if(x[i].winner in matchesown[x[i].season]){
                matchesown[x[i].season][x[i].winner] +=1
            }else{
                matchesown[x[i].season][x[i].winner] =1
            }
        }else{
            matchesown[x[i].season] = {}
            matchesown[x[i].season][x[i].winner] =1
        }
    }
    return matchesown;
}





function extraruns(x,y){
    let count =0;
    let match = {};
    let arr = [];
    for(j=0;j<x.length;j++){
        if (x[j].season === 2016){
            arr.push(x[j].id)
        }
    }
    let startmatch = Math.min(...arr);
    let endmatch =Math.max(...arr);
    for (let i=0;i<y.length;i++){
        if (y[i].match_id >=startmatch && y[i].match_id <endmatch){
            if (y[i].bowling_team in match){
                match[y[i].bowling_team] +=y[i].extra_runs
            }else{
                match[y[i].bowling_team] = y[i].extra_runs
            }
        }
    }
    return match;
}




function ecobowlers(x,y){
    let arr = [];
    for(j=0;j<x.length;j++){
        if (x[j].season === 2015){
            arr.push(x[j].id)
        }
    }
    let totalBalls = {};
    let runs = {};
    for (let i=0;i<y.length;i++){
        if (arr .includes(y[i].match_id)){
            if (y[i].bowler in totalBalls && y[i].bowler in runs){
            totalBalls[y[i].bowler] += 1;
            runs[y[i].bowler] += parseInt(y[i].total_runs);
        }else{
            totalBalls[y[i].bowler] = 1;
            runs[y[i].bowler] = parseInt(y[i].total_runs);
        }
    } 
}let totalOvers = {};
for (let players in totalBalls){
    if (totalBalls[players] % 6 === 0) {
        totalOvers[players] = totalBalls[players] / 6;
    } else {
        let extraBall = totalBalls[players] % 6;
        totalOvers[players] = ((totalBalls[players] / 6) + extraBall / 6).toFixed(2);
    }
}
let bowlerArray = [];
for (let value in totalOvers, runs) {
    bowlerArray.push([value, (runs[value] / totalOvers[value]).toFixed(2)]);
}
bowlerArray.sort(function (bowler1, bowler2) {
    return bowler1[1] - bowler2[1]
});
let economicalObj = bowlerArray.slice(0, 10);
let economybowlers = {};
for(let i=0;i<economicalObj.length;i++){
    if (economicalObj[i] in economybowlers){
        economybowlers[economicalObj[i][0]] +=  economicalObj[i][1]
    }else{
        economybowlers[economicalObj[i][0]] = economicalObj[i][1]
    }
}
return economybowlers;
}


module.exports.matchesperyear = matchesperyear;
module.exports.matchesownperyear = matchesownperyear;
module.exports.extraruns = extraruns;
module.exports.ecobowlers = ecobowlers;