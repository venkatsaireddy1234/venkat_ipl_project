const fs = require("fs");

let csvToJson = require("convert-csv-to-json");

let x =csvToJson.fieldDelimiter(",").formatValueByType().getJsonFromCsv("src/data/matches.csv");

let y =csvToJson.fieldDelimiter(",").formatValueByType().getJsonFromCsv("src/data/deliveries.csv");


const  ipl = require("./ipl.js")

let data = ipl.ecobowlers(x,y)

data = JSON.stringify(data);


fs.writeFile("src/public/output/ecobowlers.json",data, function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
 
    console.log("JSON file has been saved.");
});
